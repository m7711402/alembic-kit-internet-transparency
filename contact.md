---
title: Contact us
feature_image: "/pics/background-ga3c4abc21_1920.jpg"
---

In case our scans is impacting you and you decide to be removed from it, please contact us at the address below. We will appreciate if you can provide us with some additional information such as:

* The name of your organization and a point of contact (e-mail, and if possible a phone number);
* IP range to be out of our scans;
* Port number to be out of our scans (or all ports);
* How long you do not want your IP range to be scanned;
* Reason to be excluded from the scans (what service is being disrupted or why you feel your service is impacted).

Address: [internet-transparency@utwente.nl](mailto:internet-transparency@utwente.nl)
